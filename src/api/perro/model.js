import mongoose, { Schema } from 'mongoose'

const perroSchema = new Schema({
  nombre: {
    type: String
  },
  raza: {
    type: String
  },
  color: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

perroSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      nombre: this.nombre,
      raza: this.raza,
      color: this.color,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Perro', perroSchema)

export const schema = model.schema
export default model
