import { Perro } from '.'

let perro

beforeEach(async () => {
  perro = await Perro.create({ nombre: 'test', raza: 'test', color: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = perro.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(perro.id)
    expect(view.nombre).toBe(perro.nombre)
    expect(view.raza).toBe(perro.raza)
    expect(view.color).toBe(perro.color)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = perro.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(perro.id)
    expect(view.nombre).toBe(perro.nombre)
    expect(view.raza).toBe(perro.raza)
    expect(view.color).toBe(perro.color)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
