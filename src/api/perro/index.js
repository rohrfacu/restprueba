import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { master } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Perro, { schema } from './model'

const router = new Router()
const { nombre, raza, color } = schema.tree

/**
 * @api {post} /perros Create perro
 * @apiName CreatePerro
 * @apiGroup Perro
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam nombre Perro's nombre.
 * @apiParam raza Perro's raza.
 * @apiParam color Perro's color.
 * @apiSuccess {Object} perro Perro's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Perro not found.
 * @apiError 401 master access only.
 */
router.post('/',
  master(),
  body({ nombre, raza, color }),
  create)

/**
 * @api {get} /perros Retrieve perros
 * @apiName RetrievePerros
 * @apiGroup Perro
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of perros.
 * @apiSuccess {Object[]} rows List of perros.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 master access only.
 */
router.get('/',
  master(),
  query(),
  index)

/**
 * @api {get} /perros/:id Retrieve perro
 * @apiName RetrievePerro
 * @apiGroup Perro
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess {Object} perro Perro's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Perro not found.
 * @apiError 401 master access only.
 */
router.get('/:id',
  master(),
  show)

/**
 * @api {put} /perros/:id Update perro
 * @apiName UpdatePerro
 * @apiGroup Perro
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiParam nombre Perro's nombre.
 * @apiParam raza Perro's raza.
 * @apiParam color Perro's color.
 * @apiSuccess {Object} perro Perro's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Perro not found.
 * @apiError 401 master access only.
 */
router.put('/:id',
  master(),
  body({ nombre, raza, color }),
  update)

/**
 * @api {delete} /perros/:id Delete perro
 * @apiName DeletePerro
 * @apiGroup Perro
 * @apiPermission master
 * @apiParam {String} access_token master access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Perro not found.
 * @apiError 401 master access only.
 */
router.delete('/:id',
  master(),
  destroy)

export default router
