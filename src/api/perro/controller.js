import { success, notFound } from '../../services/response/'
import { Perro } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Perro.create(body)
    .then((perro) => perro.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Perro.count(query)
    .then(count => Perro.find(query, select, cursor)
      .then((perros) => ({
        count,
        rows: perros.map((perro) => perro.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Perro.findById(params.id)
    .then(notFound(res))
    .then((perro) => perro ? perro.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Perro.findById(params.id)
    .then(notFound(res))
    .then((perro) => perro ? Object.assign(perro, body).save() : null)
    .then((perro) => perro ? perro.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Perro.findById(params.id)
    .then(notFound(res))
    .then((perro) => perro ? perro.remove() : null)
    .then(success(res, 204))
    .catch(next)
