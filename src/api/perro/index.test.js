import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Perro } from '.'

const app = () => express(apiRoot, routes)

let perro

beforeEach(async () => {
  perro = await Perro.create({})
})

test('POST /perros 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ access_token: masterKey, nombre: 'test', raza: 'test', color: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.nombre).toEqual('test')
  expect(body.raza).toEqual('test')
  expect(body.color).toEqual('test')
})

test('POST /perros 401', async () => {
  const { status } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /perros 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /perros 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(401)
})

test('GET /perros/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${perro.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(perro.id)
})

test('GET /perros/:id 401', async () => {
  const { status } = await request(app())
    .get(`${apiRoot}/${perro.id}`)
  expect(status).toBe(401)
})

test('GET /perros/:id 404 (master)', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})

test('PUT /perros/:id 200 (master)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${perro.id}`)
    .send({ access_token: masterKey, nombre: 'test', raza: 'test', color: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(perro.id)
  expect(body.nombre).toEqual('test')
  expect(body.raza).toEqual('test')
  expect(body.color).toEqual('test')
})

test('PUT /perros/:id 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${perro.id}`)
  expect(status).toBe(401)
})

test('PUT /perros/:id 404 (master)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ access_token: masterKey, nombre: 'test', raza: 'test', color: 'test' })
  expect(status).toBe(404)
})

test('DELETE /perros/:id 204 (master)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${perro.id}`)
    .query({ access_token: masterKey })
  expect(status).toBe(204)
})

test('DELETE /perros/:id 401', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${perro.id}`)
  expect(status).toBe(401)
})

test('DELETE /perros/:id 404 (master)', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
    .query({ access_token: masterKey })
  expect(status).toBe(404)
})
